    <div class="row">
      <div class="col-sm-12">
        <div id="demo" class="carousel slide" style="width: 100%;" data-ride="carousel">

          <!-- Indicators -->
          <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
          </ul>

          <!-- The slideshow -->
          <div class="carousel-inner">
            <div class="carousel-item active" style="width: 100%;">
              <img src="http://www.gsscpa.com/wp-content/uploads/2018/02/file-2.jpg" style="width: 100%;" alt="Los Angeles">
            </div>
            <div class="carousel-item" style="width: 100%;">
              <img src="http://www.gsscpa.com/wp-content/uploads/2018/02/file-2.jpg" style="width: 100%;" alt="Chicago">
            </div>
            <div class="carousel-item" style="width: 100%;">
              <img src="http://www.gsscpa.com/wp-content/uploads/2018/02/file-2.jpg" style="width: 100%;" alt="New York">
            </div>
          </div>

          <!-- Left and right controls -->
          <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
          </a>
          <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
          </a>

        </div>
      </div>
    </div>

    <div class="row box-container">
      <div class="col-sm-4">
        <div class="box-circle">
          <img class="img img-fluid" src="<?=$tinydb[0]["text1"]?>" alt="">
        </div>
        <h2 class="title text-center"><?=$tinydb[0]["text2"]?></h2>
        <span class="description text-center"><?=$tinydb[0]["text3"]?></span>
      </div>
      <div class="col-sm-4">
        <div class="box-circle">
          <img class="img img-fluid" src="<?=$tinydb[0]["text4"]?>" alt="">
        </div>
        <h2 class="title text-center"><?=$tinydb[0]["text5"]?></h2>
        <span class="description text-center"><?=$tinydb[0]["text6"]?></span>
      </div>
      <div class="col-sm-4">
        <div class="box-circle">
          <img class="img img-fluid" src="<?=$tinydb[0]["text7"]?>" alt="">
        </div>
        <h2 class="title text-center"><?=$tinydb[0]["text8"]?></h2>
        <span class="description text-center"><?=$tinydb[0]["text9"]?></span>
      </div>
    </div>

    <div class="row box-container jumbotron">
      <div class="col-sm-12">
        <h1 class="text-center">PRODUK KAMI</h1>
      </div>
      <div class="col-sm-4 box-product">
        <div class="box-circle">
          <img class="img img-fluid" src="http://www.freakhousegraphics.com/widget/image/placeholder.png" alt="">
        </div>
      </div>
      <div class="col-sm-4 box-product">
        <div class="box-circle">
          <img class="img img-fluid" src="http://www.freakhousegraphics.com/widget/image/placeholder.png" alt="">
        </div>
      </div>
      <div class="col-sm-4 box-product">
        <div class="box-circle">
          <img class="img img-fluid" src="http://www.freakhousegraphics.com/widget/image/placeholder.png" alt="">
        </div>
      </div>

      <div class="offset-sm-2 col-sm-4 box-product">
        <div class="box-circle">
          <img class="img img-fluid" src="http://www.freakhousegraphics.com/widget/image/placeholder.png" alt="">
        </div>
      </div>
      <div class="col-sm-4 box-product">
        <div class="box-circle">
          <img class="img img-fluid" src="http://www.freakhousegraphics.com/widget/image/placeholder.png" alt="">
        </div>
      </div>
    </div>

    <div class="row box-container">
      <div class="col-sm-6" style="margin:auto;">
        <div class="row">
          <div class="offset-3 col-6 offset-sm-4 col-sm-4">
            <div class="box-circle">
              <img class="img img-fluid" src="<?=$tinydb[0]["text10"]?>" alt="">
            </div>
          </div>
          <div class="offset-sm-2 col-sm-8">
            <h2 class="title text-center"><?=$tinydb[0]["text11"]?></h2>
            <span class="description text-center"><?=$tinydb[0]["text19"]?></span>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <img class="img img-fluid" src="<?=$tinydb[0]["text12"]?>" alt="">
      </div>
    </div>

    <div class="row box-container jumbotron">
      <div class="col-sm-6" style="margin-bottom: 25px;">
        <iframe width="100%" height="300" src="<?=$tinydb[0]["text15"]?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div>
      <div class="col-sm-6" style="margin:auto;">
        <div class="row">
          <div class="offset-3 col-6 offset-sm-4 col-sm-4">
            <div class="box-circle">
              <img class="img img-fluid" src="<?=$tinydb[0]["text13"]?>" alt="">
            </div>
          </div>
          <div class="col-sm-12">
            <h1 class="title text-center"><?=$tinydb[0]["text14"]?></h1>
          </div>
        </div>
      </div>
    </div>