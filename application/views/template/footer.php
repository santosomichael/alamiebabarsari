	<div class="row box-container box-footer">
		<div class="col-12 col-sm-3">
			<h6 class="text-white"><?=$tinydb[0]["text16"]?></h6>
		</div>
		<div class="col-12 offset-sm-6 col-sm-3">
			<div class="row">
				<div class="col-3 col-sm-6">
					<a href="<?=$tinydb[0]["text17"]?>">
						<img class="img img-fluid" src="http://www.freakhousegraphics.com/widget/image/placeholder.png" alt="">
					</a>
				</div>
				<div class="col-3 col-sm-6">
					<a href="<?=$tinydb[0]["text18"]?>">
						<img class="img img-fluid" src="http://www.freakhousegraphics.com/widget/image/placeholder.png" alt="">
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </body>
<script>
	(function() {
		function toRp(angka){
	        var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
	        var rev2    = '';
	        for(var i = 0; i < rev.length; i++){
	            rev2  += rev[i];
	            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
	                rev2 += '.';
	            }
	        }
	        return 'Rp. ' + rev2.split('').reverse().join('') + ',00';
	    }
	}())	
</script>
</html>