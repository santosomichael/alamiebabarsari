  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Handicraft and Caricature</b>
    </div>
    <strong>Copyright &copy; 2017 <a href="https://adminlte.io">Peapepo</a>.</strong> All rights
    reserved.
  </footer>
  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>

<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="<?=base_url().'assets/'?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url().'assets/'?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?=base_url().'assets/'?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url().'assets/'?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url().'assets/'?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url().'assets/'?>dist/js/demo.js"></script>
<!-- DataTables -->
<script src="<?=base_url().'assets/'?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url().'assets/'?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- CK EDITOR -->
<script src="<?=base_url().'assets/'?>bower_components/ckeditor/ckeditor.js"></script>
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree();
    // $('#example1').DataTable();
    // $('#example2').DataTable({
    //   'paging'      : true,
    //   'lengthChange': false,
    //   'searching'   : false,
    //   'ordering'    : true,
    //   'info'        : true,
    //   'autoWidth'   : false
    // });
    $('#example1').dataTable({
    <?php
      $search = "";
      if (isset($_POST["filter"])) {
        for ($i = 1; $i <= $_POST["filter"]; $i++) {
          $search .= $_POST["defaultSearch$i"]." ";
        }
      } else if (isset($_GET["defaultSearch"])) {
        $search .= $_GET["defaultSearch"]." ";
      }
    ?>
      "oSearch": {"sSearch": "<?=$search?>"}
    });
    $('#example2').dataTable();
    $('#example3').dataTable();

    CKEDITOR.replace('editor1')
    CKEDITOR.replace('editor2')
    CKEDITOR.replace('editor3')
    CKEDITOR.replace('editor4')
    CKEDITOR.replace('editor5')
  })
</script>
</body>
</html>