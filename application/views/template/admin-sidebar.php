<!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div> -->
      <!-- search form -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <!-- <li><a href="<?=base_url()?>admin/home"><i class="fa fa-book"></i><span>Home</span></a></li> -->
        <li><a href="<?=base_url()?>admin/generals"><i class="fa fa-star"></i><span>General</span></a></li>
        <!-- <li><a href="<?=base_url()?>admin/orders"><i class="fa fa-shopping-cart"></i><span>Orders</span></a></li> -->
        <li><a href="<?=base_url()?>admin/products"><i class="fa fa-cube"></i><span>Products</span></a></li>
        <!-- <li><a href="<?=base_url()?>admin/inventories"><i class="fa fa-cogs"></i><span>Inventories</span></a></li> -->
        <!-- <li><a href="<?=base_url()?>admin/members"><i class="fa fa-users"></i><span>Members</span></a></li> -->
        <!-- <li><a href="<?=base_url()?>admin/discounts"><i class="fa fa-tags"></i><span>Discounts</span></a></li> -->
        <!-- <li><a href="<?=base_url()?>admin/vouchers"><i class="fa fa-money"></i><span>Vouchers</span></a></li> -->
        <!-- <li><a href="<?=base_url()?>admin/reports?month=2&year=2018&bulanan=Lihat"><i class="fa fa-file"></i><span>Reports</span></a></li> -->
        <!-- <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- ===============================================