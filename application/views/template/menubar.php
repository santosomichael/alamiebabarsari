<nav class="navbar navbar-expand-lg navbar-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <!-- <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Imsonia</a>
          </li>
        </ul> -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>about">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>products">Products</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>contact">Contact</a>
          </li>
        </ul>
      </div>
    </nav>