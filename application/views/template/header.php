<!doctype html>
<html lang="en">
<head>
  <title>Alamiebabarsari</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <style>
  body {
    background-color: white;
    font-family: 'Montserrat', sans-serif;
    padding-top:25px;
  }

  ul.navbar-nav {
    margin: auto;
  }

  .nav-item a .nav-link {
    font-size:2em;
  }

  .box-circle {
    width: 80%;
    background-color: #AAA;
    margin: auto;
    margin-bottom: 25px;
  }

  .circle-ig {
    height: 75px;
    width: 75px;
    border:2px solid black;
    background-color: #F7F7F7;
  }

  .theme-caption {
    margin-top:30px;
  }

  .box-container {
    margin-top:25px;
    padding:100px;
  }

  .box-product {
    margin-bottom:50px;
  }
  .box-footer {
    background-color: black;
  }

  .empty-icon {
    font-size: 80px;
    color: grey;
    margin-bottom:40px;
  }

  .empty-icon-container {
    width:100%; 
    margin:auto;
    text-align: center;
  }

  .empty-text {
    color: grey;
    text-align: center;
  }

</style>
</head>
<body class="container">
  <div class="row">
    <div class="offset-4 col-4 offset-sm-5 col-sm-2">
      <div class="box-circle">
        <img class="img img-fluid" src="http://www.freakhousegraphics.com/widget/image/placeholder.png" alt="">
      </div>
    </div>
  </div>