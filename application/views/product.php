    <div class="row">
      <div class="col-sm-12">
        <ul class="nav justify-content-center">
		  <li class="nav-item">
		    <a class="nav-link" href="<?=base_url()?>products/Mie Bumbu">Mie Bumbu</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="<?=base_url()?>products/Mie Cup Bumbu">Mie Cup Bumbu</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="<?=base_url()?>products/Mie Non Bumbu">Mie Non Bumbu</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="<?=base_url()?>products/Mie Penyedap">Mie Penyedap</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="<?=base_url()?>products/Stick">Stick</a>
		  </li>
		</ul>
      </div>
    </div>
    <div class="row box-container jumbotron">
        <?php
            foreach ($products as $product) {
                if ($product['cpic'] == NULL)
                    $cpic = "http://www.freakhousegraphics.com/widget/image/placeholder.png";
                else $cpic = $product['cpic'];
        ?>
            <div class="col-sm-3 box-product">
                <div class="box-circle">
                    <img class="img img-fluid" src="<?=$cpic?>" alt="">
                </div>
                <h6 class="product-caption"><?=$product['cproductname']?></h6>
                <h6 class="product-price"><?="Rp. ".number_format($product['nprice'], 2, ",", ".")?></h6>
            </div>
        <?php
            }
        ?>
        
        <?php
            if (count($products) == 0) {
        ?>
                <div class="col-sm-12">
                    <div class="empty-icon-container">
                        <i class="fa fa-times-circle empty-icon"></i>
                    </div>
                    <h4 class="empty-text">No item found with type <?=$productType?> </h4>
                </div>
        <?php        
            }
        ?>
        

    	<!-- <div class="col-sm-3 box-product">
    		<div class="box-circle">
    			<img class="img img-fluid" src="http://www.freakhousegraphics.com/widget/image/placeholder.png" alt="">
    		</div>
    		<h6 class="product-caption">Camilan Stick Bayam Hijau</h6>
    		<h6 class="product-price">Rp. 15.000,00</h6>
    	</div>
    	<div class="col-sm-3 box-product">
    		<div class="box-circle">
    			<img class="img img-fluid" src="http://www.freakhousegraphics.com/widget/image/placeholder.png" alt="">
    		</div>
    		<h6 class="product-caption">Camilan Stick Bayam Hijau</h6>
    		<h6 class="product-price">Rp. 15.000,00</h6>
    	</div>
    	<div class="col-sm-3 box-product">
    		<div class="box-circle">
    			<img class="img img-fluid" src="http://www.freakhousegraphics.com/widget/image/placeholder.png" alt="">
    		</div>
    		<h6 class="product-caption">Camilan Stick Bayam Hijau</h6>
    		<h6 class="product-price">Rp. 15.000,00</h6>
    	</div>
    	<div class="col-sm-3 box-product">
    		<div class="box-circle">
    			<img class="img img-fluid" src="http://www.freakhousegraphics.com/widget/image/placeholder.png" alt="">
    		</div>
    		<h6 class="product-caption">Camilan Stick Bayam Hijau</h6>
    		<h6 class="product-price">Rp. 15.000,00</h6>
    	</div>
    	<div class="col-sm-3 box-product">
    		<div class="box-circle">
    			<img class="img img-fluid" src="http://www.freakhousegraphics.com/widget/image/placeholder.png" alt="">
    		</div>
    		<h6 class="product-caption">Camilan Stick Bayam Hijau</h6>
    		<h6 class="product-price">Rp. 15.000,00</h6>
    	</div>
    	<div class="col-sm-3 box-product">
    		<div class="box-circle">
    			<img class="img img-fluid" src="http://www.freakhousegraphics.com/widget/image/placeholder.png" alt="">
    		</div>
    		<h6 class="product-caption">Camilan Stick Bayam Hijau</h6>
    		<h6 class="product-price">Rp. 15.000,00</h6>
    	</div>
    	<div class="col-sm-3 box-product">
    		<div class="box-circle">
    			<img class="img img-fluid" src="http://www.freakhousegraphics.com/widget/image/placeholder.png" alt="">
    		</div>
    		<h6 class="product-caption">Camilan Stick Bayam Hijau</h6>
    		<h6 class="product-price">Rp. 15.000,00</h6>
    	</div>
    	<div class="col-sm-3 box-product">
    		<div class="box-circle">
    			<img class="img img-fluid" src="http://www.freakhousegraphics.com/widget/image/placeholder.png" alt="">
    		</div>
    		<h6 class="product-caption">Camilan Stick Bayam Hijau</h6>
    		<h6 class="product-price">Rp. 15.000,00</h6>
    	</div> -->
    </div>