<?php
	$folder="admin";
	$controller="discounts";
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
    <?php if (isset($_SESSION['alert'])) {?>

	    <?php 
	    	$alert = "success";
	    	if (isset($_SESSION['error'])) {
	    		$alert = "danger";
	    	}
	    ?>
	  	<div class="box box-<?= $alert; ?> box-solid">
	        <div class="box-header with-border">
	          <h3 class="box-title">Alert</h3>

	          <!-- <div class="box-tools pull-right">
	            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
	          </div> -->
	          <!-- /.box-tools -->
	        </div>
	        <!-- /.box-header -->
	        <div class="box-body">
	          <strong><?=$_SESSION['alert']?></strong>
	        </div>
	    </div>
	<?php } ?>
      <div class="box">
        <div class="box-body">
        	<?php
        		if ($action == "edit") {
        	?>
	        		<?=form_open(uri_string(), [])?>

	        			<!-- <?php
	        				$fieldsTitle = array("Penjelasan Produk", "Dapur", "Video", "Footer");
	        				$fieldsName = array("Gambar 1", "Judul 1", "Deskripsi 1", "Gambar 2", "Judul 2", "Deskripsi 2", "Gambar 3", "Judul 3", "Deskripsi 3", "Gambar", "Title", "Gambar Besar", "Gambar Liputan", "Deskripsi", "Link Video", "Deskripsi Kiri", "Link IG", "Link Facebook");
	        				$fieldsType = array(1,1,2,1,1,2,1,1,2,1,1,1,1,2,1,2,1,1);

	        				$index = 0;
	        				$indexFieldsTitle = 0;
	        				foreach ($fieldsName as $fieldName) {
	        					$temp = $index+1;
	        					if ($index == 0 ||  $index == 10 || $index == 13 || $index == 16) {
	        						echo "<label>$fieldsTitle[$indexFieldsTitle]</label>";
	        						$indexFieldsTitle++;
	        					}
	        			?>
	        				<div class="form-group">
	        					<label><?=$fieldName?></label>
	        					<?php if ($fieldsType[$index] == 1) { ?>
	        					<input type="text" class="form-control" name="values[text<?=($index+1)?>]" value="<?=$items[0][$temp]?>" />
	        					<?php } else { ?>
								<textarea class="form-control" id="editor1" name="values[text<?=($index+1)?>]" cols="30" rows="10"><?=$items[0][$temp]?></textarea>
	        					<?php } ?>
	        				</div>
						<?php

	        					$index++;
	        				}
	        			?> -->
	        			<h3>Penjelasan Produk</h3>
		        		<div class="form-group">
			        		<label>Gambar 1</label>
			        		<input type="text" class="form-control" name="values[text1]" value="<?=$items[0]['text1']?>" />
		        		</div>
		        		<div class="form-group">
			        		<label>Judul 1</label>
			        		<input type="text" class="form-control" name="values[text2]" value="<?=$items[0]['text2']?>" />
		        		</div>
		        		<div class="form-group">
			        		<label>Deskripsi 1</label>
			        		<textarea class="form-control" id="editor1" name="values[text3]" cols="30" rows="10"><?=$items[0]['text3']?></textarea>
		        		</div>
		        		<div class="form-group">
			        		<label>Gambar 2</label>
			        		<input type="text" class="form-control" name="values[text4]" value="<?=$items[0]['text4']?>" />
		        		</div>
		        		<div class="form-group">
			        		<label>Judul 2</label>
			        		<input type="text" class="form-control" name="values[text5]" value="<?=$items[0]['text5']?>" />
		        		</div>
		        		<div class="form-group">
			        		<label>Deskripsi 2</label>
			        		<textarea class="form-control" id="editor2" name="values[text6]" cols="30" rows="10"><?=$items[0]['text6']?></textarea>
		        		</div>
						<div class="form-group">
			        		<label>Gambar 3</label>
			        		<input type="text" class="form-control" name="values[text7]" value="<?=$items[0]['text7']?>" />
			        	</div>
			        	<div class="form-group">
			        		<label>Judul 3</label>
			        		<input type="text" class="form-control" name="values[text8]" value="<?=$items[0]['text8']?>" />
			        	</div>
			        	<div class="form-group">
			        		<label>Deskripsi 3</label>
			        		<textarea class="form-control" id="editor3" name="values[text9]" cols="30" rows="10"><?=$items[0]['text9']?></textarea>
			        	</div>
						
						<h3>Dapur</h3>
						<div class="form-group">
			        		<label>Gambar</label>
			        		<input type="text" class="form-control" name="values[text10]" value="<?=$items[0]['text10']?>" />
			        	</div>
			        	<div class="form-group">
			        		<label>Title</label>
			        		<input type="text" class="form-control" name="values[text11]" value="<?=$items[0]['text11']?>" />
			        	</div>
			        	<div class="form-group">
			        		<label>Deskripsi</label>
			        		<textarea class="form-control" id="editor4" name="values[text19]" cols="30" rows="10"><?=$items[0]['text19']?></textarea>
			        	</div>
			        	<div class="form-group">
			        		<label>Gambar Besar</label>
			        		<input type="text" class="form-control" name="values[text12]" value="<?=$items[0]['text12']?>" />
			        	</div>

			        	<h3>Video</h3>
			        	<div class="form-group">
			        		<label>Gambar Liputan</label>
			        		<input type="text" class="form-control" name="values[text13]" value="<?=$items[0]['text13']?>" />
			        	</div>
			        	<div class="form-group">
			        		<label>Deskripsi</label>
			        		<textarea class="form-control" id="editor4" name="values[text14]" cols="30" rows="10"><?=$items[0]['text14']?></textarea>
			        	</div>

			        	<div class="form-group">
			        		<label>Link Video</label>
			        		<input type="text" class="form-control" name="values[text15]" value="<?=$items[0]['text15']?>" />
			        	</div>

			        	<h3>Footer</h3>
			        	<div class="form-group">
			        		<label>Deskripsi Kiri</label>
			        		<textarea class="form-control" id="editor5" name="values[text16]" cols="30" rows="10"><?=$items[0]['text16']?></textarea>
			        	</div>
			        	<div class="form-group">
			        		<label>Link IG</label>
			        		<input type="text" class="form-control" name="values[text17]" value="<?=$items[0]['text17']?>" />
			        	</div>

			        	<div class="form-group">
			        		<label>Link Facebook</label>
			        		<input type="text" class="form-control" name="values[text18]" value="<?=$items[0]['text18']?>" />
			        	</div>
		        		
						<input type="submit" name="submit" class="btn btn-success" value="Submit" />
    		    	</form>
        	<?php
        		} else {
        	?>
				<div class="form-group">
					<a href="<?=current_url()?>/edit" class="btn btn-warning">Edit <i class="fa fa-pencil"></i></a>
	        		<h3>Penjelasan Produk</h3>
	        		<label>Gambar Produk 1</label>
	        		<h4><?=$items[0]["text1"]?></h4>
	        		<br>
	        		<label>Judul Produk 1</label>
	        		<h4><?=$items[0]["text2"]?></h4>
	        		<br>
	        		<label>Deskripsi Produk 1</label>
	        		<h4><?=$items[0]["text3"]?></h4>
	        		<br>
					
					<br>

	        		<label>Gambar Produk 2</label>
	        		<h4><?=$items[0]["text4"]?></h4>
	        		<br>
	        		<label>Judul Produk 2</label>
	        		<h4><?=$items[0]["text5"]?></h4>
	        		<label>Deskripsi Produk 2</label>
	        		<h4><?=$items[0]["text6"]?></h4>
	        		<br>

	        		<label>Gambar Produk 3</label>
	        		<h4><?=$items[0]["text7"]?></h4>
	        		<br>
	        		<label>Judul Produk 3</label>
	        		<h4><?=$items[0]["text8"]?></h4>
	        		<br>
	        		<label>Deskripsi Produk 3</label>
	        		<h4><?=$items[0]["text9"]?></h4>
	        		<br>
					
					<br>

					<hr />
	        		<h3>Dapur</h3>
	        		<h4><?=$items[0]["text10"]?></h4>
	        		<br>
	        		<label>Gambar</label>
	        		<h4><?=$items[0]["text11"]?></h4>
	        		<br>
	        		<label>GambarBesar</label>
	        		<h4><?=$items[0]["text12"]?></h4>
	        		<br>
	        		
	        		<hr />
					<h3>Video</h3>
	        		<label>Gambar Liputan</label>
	        		<h4><?=$items[0]["text13"]?></h4>
	        		<br>
	        		<label>Deskripsi</label>
	        		<p><?=$items[0]["text14"]?></p>
	        		<br>
	        		<label>Link Youtube</label>
	        		<p><?=$items[0]["text15"]?></p>
	        		<br>

	        		<hr />
					<h3>Footer</h3>
	        		<label>Deskripsi Kiri</label>
	        		<h4><?=$items[0]["text16"]?></h4>
	        		<br>
	        		<label>Link IG</label>
	        		<p><?=$items[0]["text17"]?></p>
	        		<br>
	        		<label>Link Facebook</label>
	        		<p><?=$items[0]["text18"]?></p>
	        		<br>


	        	</div>
        	<?php
        		}
        	?>
        	
		</div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->