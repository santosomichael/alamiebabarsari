<?php
	$listLabels = array('No', 'Name', 'Price', 'Jenis', 'Aktif', 'Action');
	$listItems = array('cproductname', 'nprice', 'cproducttypepk', 'baktif');
	$fieldsLabel = array('Name', 'Price', 'Type', "Aktif");
	$fieldsType = array('text', 'number', "checkbox");
	$fieldsName = array('cproductname', 'nprice', 'baktif');
	$fieldsDisabled = array(0,0,0);
	$fieldsValue = array();

	$i = 0;
	foreach ($fieldsName as $field) {
		if (isset($item)) {
			if ($item[0][$field] == 1)
				array_push($fieldsValue, "checked");
			else array_push($fieldsValue, $item[0][$field]);
		} else {
			array_push($fieldsValue, "");
		}
		$i++;
	}

	$folder="admin";
	$controller="products";
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
    <?php if (isset($_SESSION['alert'])) {?>
	  	<div class="box box-success box-solid">
	        <div class="box-header with-border">
	          <h3 class="box-title">Alert</h3>

	          <!-- <div class="box-tools pull-right">
	            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
	          </div> -->
	          <!-- /.box-tools -->
	        </div>
	        <!-- /.box-header -->
	        <div class="box-body">
	          <strong><?=$_SESSION['alert']?></strong>
	        </div>
	    </div>
	<?php } ?>
      <div class="box">
        <div class="box-body">
	<?php if ($action == "list") { ?>

        	<!-- <div class="box box-danger">
		        <div class="box-header with-border">
		          <h3 class="box-title">Filter</h3>

		          <div class="box-tools pull-right">
		            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
		            </button>
		          </div>
		        </div>
		        <div class="box-body">
		          <div class="row">
		          	<?=form_open(uri_string(), [])?>
	            	<input type="hidden" name="filter" value="1">
		            <div class="col-xs-12 col-sm-4">
		            	<form action="">
		            		<div class="form-group">
			            		<label>Product</label>
			            		<select class="form-control" name="defaultSearch1">
			            			<option value=''>--</option>
			            			<?php
			            				foreach ($items as $product) {
			            					$selected="";
			            					if ($_POST["defaultSearch1"] == $product['cprodname']) {
			            						$selected="selected";
			            					}
			            					echo "<option value='".$product['cprodname']."' $selected> ".$product['cprodname']."</option>";
			            				}
			            			?>
			            		</select>
			            	</div>
			            	<div>
			            		<input class="btn btn-primary" type="submit" value="Filter">
			            	</div>
		            	</form>
		            </div>
		          </div>
		        </div>
		    </div> -->
			<table id="example1" class="table table-bordered table-striped">
				<thead>
					<?php
						foreach ($listLabels as $listLabel) {
					?>
							<td><?=$listLabel?></td>
					<?php
						}
					?>
				</thead>
	<?php
		$i = 1;
		foreach ($items as $item) {
	?>

				<tr>
					<td><?=$i?></td>
					<?php
						foreach ($listItems as $listItem) {
							if ($listItem == "baktif") {
								if ($item[$listItem]) 
									echo "<td>Aktif</td>";
								else echo "<td>Tidak Aktif</td>";
							} else if ($listItem == "nprice" || $listItem == 'nbasehpp') {
								echo "<td> Rp.".number_format($item[$listItem], 1, ",", ".")."</td>";
							} else {
								echo "<td>".$item[$listItem]."</td>";
							}
						}
					?>
					
					<td>
						<a href="<?=current_url()?>/edit/<?=$item[$id]?>" class="btn btn-warning btn-small"><i class="fa fa-pencil"></i></a>
						<a href="<?=current_url()?>/delete/<?=$item[$id]?>" class="btn btn-danger btn-small" onClick='return confirm("Apakah anda yakin?");'><i class="fa fa-trash"></i></a>
					</td>
				</tr>
	<?php
			$i++;
		}
	?>
				<tfoot>
					<?php
						foreach ($listLabels as $listLabel) {
					?>
							<td><?=$listLabel?></td>
					<?php
						}
					?>
				</tfoot>
			</table>
			<a href="<?=current_url()?>/create" class="btn btn-primary">Create <i class="fa fa-plus"></i></a>
	<?php
		} else if ($action == "create" || $action == "edit") {
	?>		
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
	              <li class="active"><a href="#product" data-toggle="tab">Product</a></li>
	              <?php if ($action == "edit") { ?>
	              	
	              <?php } ?>
	            </ul>
				<div class="tab-content">
              		<div class="active tab-pane" id="product">
						<?=form_open_multipart(uri_string(), ['class' => 'form-horizontal'])?>
							<?php
								$i = 0;
								foreach ($fieldsName as $fieldName) {
									$disabled = $fieldsDisabled[$i] ? "disabled" : false;
									if ($fieldsType[$i] == "text") {
							?>
										<div class="form-group">
											<label class="col-sm-2 control-label"><?=$fieldsLabel[$i]?></label>
											<div class="col-sm-10">
												<input type="<?=$fieldsType[$i]?>" class="form-control" name="values[<?=$fieldName?>]" value="<?=$fieldsValue[$i]?>" <?=$disabled?> />
											</div>
										</div>
							<?php
									} else if ($fieldsType[$i] == "number") {
							?>
										<div class="form-group">
											<label class="col-sm-2 control-label"><?=$fieldsLabel[$i]?></label>
											<div class="col-sm-10">
												<input type="<?=$fieldsType[$i]?>" class="form-control" name="values[<?=$fieldName?>]" value="<?=$fieldsValue[$i]?>" <?=$disabled?> />
											</div>
										</div>
							<?php
									} else if ($fieldsType[$i] == "date") {
							?>
										<div class="form-group">
											<label class="col-sm-2 control-label"><?=$fieldsLabel[$i]?></label>
											<div class="col-sm-10">
												<input type="<?=$fieldsType[$i]?>" class="form-control" name="values[<?=$fieldName?>]" value="<?=$fieldsValue[$i]?>" <?=$disabled?> />
											</div>
										</div>
							<?php
									} else if ($fieldsType[$i] == "checkbox") {
							?>
										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10">
						                      	<div class="checkbox">
							                        <label>
							                            <input type="hidden" name="values[<?=$fieldName?>]" value="0" /> 
							                            <input type="checkbox" class="form-check-input" name="values[<?=$fieldName?>]" checked <?=$fieldsValue[$i]?> value="1" <?=$disabled?> /> Aktif 
					                            	</label>
					                            </div>
					                        </div>
										</div>
							<?php
									}
									$i++;
								}
							?>

							<div class="form-group">
								<label class="col-sm-2 control-label">Product Type</label>
								<div class="col-sm-10">
									<select class="form-control" name="values[cproducttypepk]">
										<option value="Mie Bumbu">Mie Bumbu</option>
										<option value="Mie Cup Bumbu">Mie Cup Bumbu</option>
										<option value="Mie Non Bumbu">Mie Non Bumbu</option>
										<option value="Bumbu Penyedap">Bumbu Penyedap</option>
									</select>
								</div>
							</div>
							<?php if (isset($item[0]['cpic'])) { ?>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<img src="<?=base_url().$item[0]['cpic']?>">
									</div>
								</div>
							<?php } ?>

							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
		                      		<div class="checkbox">
										<input type="file" name="cpic" size="20" />
									</div>
								</div>
							</div>

							<div class="form-group">
                    			<div class="col-sm-offset-2 col-sm-10">
							    	<input class="btn btn-primary" type="submit" name="submit" value="Submit" />
							    	<a href="<?=site_url($folder.'/'.$controller)?>" class="btn btn-warning">Cancel</a>
							    </div>
						    </div>
						</form>	
					</div>
				</div>
			</div>
	<?php
		}
	?>
		</div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->