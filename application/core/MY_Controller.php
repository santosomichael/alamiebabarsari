<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        // Configuration & other initializations
	}
}

class MY_Admin extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        // Configuration & other initializations
        if ($this->uri->uri_string() !== 'admin/login' && !$this->session->username)
        	redirect('admin/login');
	}
}