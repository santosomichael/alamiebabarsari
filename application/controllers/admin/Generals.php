<?php
class Generals extends MY_Admin {
	var $table_name;
	var $page;
	var $table_pk;
	public function __construct()
	{
		parent::__construct();
        
		$this->page = "General";
		$this->page_route = "generals";
		$this->table_name = "tinydb";
		$this->table_pk = "tinydb";
        $this->load->view('template/admin-header');
		$this->load->view('template/admin-sidebar');
	}

	public function index() {
		$data['action'] = "";
		$data['id'] = $this->table_pk;
		$data['title'] = $this->page.' '.$data['action'];

		$this->db->where('baktif', 1);
		$data['items'] = $this->db->get($this->table_name)->result_array();

		$this->load->view('admin/'.$this->page_route, $data);
		$this->load->view('template/admin-footer');
	}

	public function create() {
		if (count($_POST) > 0) {
			$this->session->set_flashdata('alert', 'Create Success!');

			$this->db->insert($this->table_name, $this->input->post('values'));
			redirect("admin/".$this->page_route);
		}

		$data['action'] = "create";
		$data['id'] = $this->table_pk;
		$data['title'] = $this->page.' '.$data['action'];

		$this->load->view('admin/'.$this->page_route, $data);
		$this->load->view('template/admin-footer');
	}

	public function edit() {
		if (count($_POST) > 0) {
			$this->session->set_flashdata('alert', 'Update Success!');	
			// $this->db->where($this->table_pk, $id);
			$this->db->update($this->table_name,$this->input->post('values'));
			redirect("admin/".$this->page_route);
			
		}

		$data['action'] = "edit";
		$data['id'] = $this->table_pk;
		$data['title'] = $this->page.' '.$data['action'];
		$this->db->where('baktif', 1);
		$data['items'] = $this->db->get($this->table_name)->result_array();

		$this->load->view('admin/'.$this->page_route, $data);
		$this->load->view('template/admin-footer');
	}

	public function delete($id) {
		$this->db->set('baktif', 0);
		$this->db->where($this->table_pk, $id);
		$this->db->update($this->table_name);
		$this->session->set_flashdata('alert', 'Delete Success!');
		redirect("admin/".$this->page_route);
	}
}