<?php
class Products extends MY_Admin {
	var $table_name;
	var $page;
	var $table_pk;
	public function __construct()
	{
		parent::__construct();
        
		$this->page = "Product";
		$this->table_name = "mtproduct";
		$this->table_pk = "cproductpk";
        $this->load->view('template/admin-header');
		$this->load->view('template/admin-sidebar');
	}

	public function index() {
		$data['action'] = "list";
		$data['id'] = $this->table_pk;
		$data['title'] = $this->page.' '.$data['action'];
		$data['items'] = $this->db->get($this->table_name)->result_array();

		$this->load->view('admin/products', $data);
		$this->load->view('template/admin-footer');
	}

	public function create() {
		if (count($_POST) > 0) {
			$this->db->insert($this->table_name,$this->input->post('values'));
			
			$imageName = mysqli_insert_id($con);

			$config['upload_path']          = 'assets/images/products/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['file_name']     		= $imageName;
            $config['overwrite']     		= TRUE;
            // $config['max_size']             = 100;
            // $config['max_width']            = 1024;
            // $config['max_height']           = 768;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('cpic')) {
                $error = array('error' => $this->upload->display_errors());
                echo $error;
            }

            $this->session->set_flashdata('alert', 'Create Success!');

			redirect("admin/products");
		}

		$data['action'] = "create";
		$data['id'] = $this->table_pk;
		$data['title'] = $this->page.' '.$data['action'];

		$this->load->view('admin/products', $data);
		$this->load->view('template/admin-footer');
	}

	public function edit($id) {
		if (count($_POST) > 0) {
			$config['upload_path']          = 'assets/images/products/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['file_name']     		= $id;
            $config['overwrite']     		= TRUE;
            // $config['max_size']             = 100;
            // $config['max_width']            = 1024;
            // $config['max_height']           = 768;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('cpic')) {
                    $error = array('error' => $this->upload->display_errors());
                    echo $error;
            } else {
            	$this->db->where($this->table_pk, $id);
            	$data["cpic"] = $config['upload_path'].$this->upload->data("file_name");
				$this->db->update($this->table_name,$data);
            }

			$this->session->set_flashdata('alert', 'Update Success!');
			$this->db->where($this->table_pk, $id);
			$this->db->update($this->table_name,$this->input->post('values'));
			redirect("admin/products");
		}

		$data['action'] = "edit";
		$data['id'] = $this->table_pk;
		$data['title'] = $this->page.' '.$data['action'];
		$this->db->where($this->table_pk, $id);
		$data['item'] = $this->db->get($this->table_name)->result_array();

		$this->load->view('admin/products', $data);
		$this->load->view('template/admin-footer');
	}

	public function delete($id) {
		$this->db->set('baktif', 0);
		$this->db->where($this->table_pk, $id);
		$this->db->update($this->table_name);
		$this->session->set_flashdata('alert', 'Delete Success!');
		redirect("admin/products");
	}

	public function orders() {
		if (count($_POST) > 0) {
			$this->session->set_flashdata('alert', 'Update Success!');
			
			// get current quantity product-material
			$this->db->where('cgoodspk', $this->input->post('values[cgoodspk]'));
			$this->db->where('cprodpk', $this->input->post('values[cprodpk]'));
			$data = $this->db->get('mtproductgoods')->result_array();


			// delete current quantity product-material
			$this->db->where('cgoodspk', $this->input->post('values[cgoodspk]'));
			$this->db->where('cprodpk', $this->input->post('values[cprodpk]'));
			$this->db->delete('mtproductgoods');
			
			$values = $this->input->post('values');
			
			// insert new quantity product-material
			if (count($data) > 0)
				$values["ngoodsqty"] += $data[0]['ngoodsqty'];

			$this->db->insert("mtproductgoods",$values);
			redirect("admin/products/edit/".$this->input->post('values[cprodpk]'));
		}		
	}

	public function materialDelete($id, $productgoodspk) {
		$this->session->set_flashdata('alert', 'Delete material Success!');
		$this->db->where('cgoodspk', $productgoodspk);
		$this->db->where('cprodpk', $id);
		$this->db->delete('mtproductgoods');
		redirect("admin/products/edit/".$id);
	}
}