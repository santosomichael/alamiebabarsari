<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
        
        $this->load->view('template/header');
		$this->load->view('template/menubar');
		$this->table_name = "tinydb";
	}

	public function index()
	{
		$this->db->where('baktif', 1);
		$data['tinydb'] = $this->db->get($this->table_name)->result_array();
        // $this->db->where('baktif', 1);
        // $data['products'] = $this->db->get("mtproduct")->result_array();
		$this->load->view('index', $data);
		$this->load->view('template/footer');
	}
	public function about()
	{
		$this->db->where('baktif', 1);
		$data['tinydb'] = $this->db->get($this->table_name)->result_array();
        // $this->db->where('baktif', 1);
        // $data['products'] = $this->db->get("mtproduct")->result_array();
		$this->load->view('about', $data);
		$this->load->view('template/footer');
	}
	public function products($productType = "Mie Non Bumbu")
	{
		$productType = explode('%20',$productType);
		$productType = implode(' ',$productType);

		$this->db->where('baktif', 1);
		$data['tinydb'] = $this->db->get($this->table_name)->result_array();
        
        $this->db->where('baktif', 1);
        $this->db->where('cproducttypepk', $productType);
        $data['products'] = $this->db->get("mtproduct")->result_array();
        $data['productType'] = $productType;

		$this->load->view('product', $data);
		$this->load->view('template/footer');
	}
	public function contact()
	{
		$this->db->where('baktif', 1);
		$data['tinydb'] = $this->db->get($this->table_name)->result_array();
        // $this->db->where('baktif', 1);
        // $data['products'] = $this->db->get("mtproduct")->result_array();
		$this->load->view('contact', $data);
		$this->load->view('template/footer');
	}
}
